#include <stdlib.h>
#include <stdio.h>
#include <alloc.h>
#include <ctype.h>
#include <time.h>
#include <mem.h>
#include <dos.h>
#include "7186e.h"
#include "Tcpip32.h"
#include "MFW.h"
#include "version.h"

#define BUFSIZE 1460        /* read/write buffer size */
#define COM_SIZE 128
#define WEGSIZE 0x20

typedef struct{
DWORD ts;
DWORD we;
char dt[0x20];
}WEIGHINFO, *pWEIGHINFO;


void Web_fun( int skt , int mode);
void CallBackCom(pCOMPORT com, int mode);

BYTE * Answer;
char BufCom[COM_SIZE];
int Socket0=-1; 
extern int errno;
int isMode=0; 
long TimeZone=7L;

WEIGHINFO wi[WEGSIZE]={0};
BYTE Index=0;

TCP_SERVER TcpServer={
    8080,      
    -1,     
    0,      
    0,      
    Web_fun  // CallBackFun
};


COMPORT ComRec={
    2,              //int Port; // 0:COM0, 1:COM1,...
    BufCom,           //char *Buf;
    COM_SIZE,      //unsigned Bufsize;
    0xA,              //char EndChar; 
    64,             //unsigned TriggerLevel;
    200,             //long Timeout;
    0,              //long StartTimeTicks;
    0,              //long LastTimeTicks;
    0,              //long MasterTimeout;
    0,              //long MasterStartTimeTicks; 
    0,              //unsigned Size; 
    ReadComn_2,     //int (*ReadComn)(unsigned char *buf,int n);         //defined in 7186E.H
    CallBackCom,   //void (*CallbackFun)(pCOMPORT, int mode);
    0,              //pCOMPORT next;
};



DWORD getlocaltime(char* tmstr)
{
struct tm ti;
GetTime(&ti.tm_hour,&ti.tm_min,&ti.tm_sec);
GetDate(&ti.tm_year,&ti.tm_mon,&ti.tm_mday);
sprintf(tmstr,"%02u.%02u.%04u %02u:%02u:%02u:000",
ti.tm_mday,ti.tm_mon,ti.tm_year,ti.tm_hour,ti.tm_min,ti.tm_sec);
ti.tm_wday=GetWeekDay();
ti.tm_year-=1900;
ti.tm_mon-=1;
ti.tm_isdst=0;
return (mktime(&ti)-(TimeZone*3600L));
}

unsigned long str2long(unsigned char *Buf)
{
int i=0;
unsigned long rc=0UL;
int sLen;
sLen=(int)strlen((const char *)Buf);
	for(i=0;i<sLen;i++)
		if(isdigit(Buf[i]))
			rc = (rc*10)+(Buf[i]-'0');

return rc;	
}
int _copy_str_until(char * dst, char * src, char chr)
{
 int i, rc=0;
        for(i=0;i<64; i++)
        {
            if((src[i]==chr) || (!src[i]))
            {
                dst[i]=0;
                rc=i;
                break;
            }
         dst[i]=src[i];
        }
if(!rc)dst[0]=0;
return rc;
}
int _insert_into_string(char *dst, char *src, int into)
{
int i=0;
	for(;;)
	{
	if((!src[i]) || (!dst[i+into]))
		break;
	dst[i+into]=src[i++];
	}
return i;
}
char *GetModuleName(void)   // User's program must support this function for UDP search
{
    return "uPAC-7186EX" ;
}

char *GetAliasName(void)    // User's program must support this function for UDP search 
{
    return "Mill450" ;
}

int getCmdNum(char *p, char *cmd, char * _ar)
{
if(p==NULL)
	return 0;
if(!strnicmp(p,"?time=",6))
	{
        _copy_str_until(cmd,p+1, '=');
	_copy_str_until(_ar,p+6, ' ');
	return 1;
	}
if(!strnicmp(p,"?time",5))
	{
        _copy_str_until(cmd,p+1, ' ');
	_ar[0]=0;
	return 2;
	}
if(!strnicmp(p,"?index=",7))
	{
        _copy_str_until(cmd,p+1, '=');
	_copy_str_until(_ar,p+7, ' ');
	return 3;
	}

if(!strnicmp(p,"?sync=",6))
	{
        _copy_str_until(cmd,p+1, '=');
	_copy_str_until(_ar,p+6, ' ');
	return 11;
	}
if(!strnicmp(p,"?sync",5))
	{
        _copy_str_until(cmd,p+1, ' ');
	_ar[0]=0;
	return 12;
	}
if(!strnicmp(p,"?tz=",4))
	{
        _copy_str_until(cmd,p+1, '=');
	_copy_str_until(_ar,p+4, ' ');
	return 21;
	}
cmd[0]=_ar[0]=0;
return 0;
}

void printHead(int skt,int err)
{
unsigned char Ip[4];
struct tm ti;

memmove(Ip,&SOCKET_IPADDR(skt),4);
GetTime(&ti.tm_hour,&ti.tm_min,&ti.tm_sec);
GetDate(&ti.tm_year,&ti.tm_mon,&ti.tm_mday);

TcpPrint(skt, 0, "HTTP/1.1 200 OK\r\nContent-type: text/xml\r\nContent-Encoding: identity\r\n");
TcpPrint(skt, 0, "Allow: GET\r\nConnection: Keep-Alive\r\nServer: mPack-7186EXD (miniOS/7)\r\nKeep-Alive: timeout=2, max=10\r\nHost: mill450.zsmk.ru\r\n\r\n");
TcpPrint(skt, 0, "<?xml version=\"1.0\" encoding=\"windows-1251\"?>");
TcpPrint(skt, 0, "<root><Exp>0</Exp><Now>%lu</Now><dt>%02u.%02u.%04u %02u:%02u:%02u</dt><tz>%ld</tz><Cerr>%d</Cerr>",getlocaltime((char *)Answer),ti.tm_mday,ti.tm_mon,ti.tm_year,ti.tm_hour,ti.tm_min,ti.tm_sec,TimeZone,err);
TcpPrint(skt, 0, "<remote>%u.%u.%u.%u</remote>",Ip[0],Ip[1],Ip[2],Ip[3]);
TcpPrint(skt, 0, "<Buffer>%40.40s</Buffer>",BufCom);
TcpPrint(skt, 0, "<Ver>Release. %s</Ver>",_RELEASE_);
}

void DoDateTime(char *ar)
{
int hr,m,sec,cc,dd,mm,yy;
time_t rwtime;
struct tm *inf;
char tmp[0x10];
if(!ar[0])
	return;
if((ar[1]==':') || (ar[2]==':'))
	{
//	Print("%s\t",ar);
	cc=_copy_str_until(tmp,ar, ':')+1;
	hr=(int)str2long((unsigned char *)tmp) % 24;
//	Print("%s->%d\t",tmp,hr);
	cc+=_copy_str_until(tmp,ar+cc, ':')+1;
	m=(int)str2long((unsigned char *)tmp) % 60;
//	Print("%s->%d\t",tmp,m);
	cc+=_copy_str_until(tmp,ar+cc, ':');
	sec=(int)str2long((unsigned char *)tmp) % 60;
//	Print("%s->%d\n",tmp,sec);
	SetTime(hr, m, sec);
	}
else{
	rwtime=(time_t) str2long((unsigned char *)ar)+(TimeZone*3600L);
	inf = gmtime((const time_t*) &rwtime);
	dd=inf->tm_mday;
	mm=inf->tm_mon+1;
	yy=inf->tm_year+1900;
	hr=inf->tm_hour;
	m=inf->tm_min;
	sec=inf->tm_sec;
	SetTime(hr, m, sec);
        SetDate(yy,mm,dd);
	}
}

void DoWeight(int skt, int rc, char *tim)
{
int i;
DWORD pat;
i=Index;
if(rc > 10) return;

if((rc==1) && (!tim[0]))
	rc=2;

if(rc==1)
        pat=str2long((BYTE *)tim);

if((rc==3) && (tim[0]) && (tim[0]!='-'))
	{
        i=(int)str2long((BYTE *)tim);
 	i--;
	i&=(WEGSIZE-1);
	TcpPrint(skt, 0,"<wi><idx>%02d</idx><dt>%s</dt><ts>%lu</ts><we>%lu</we></wi>",i+1,wi[i].dt,wi[i].ts,wi[i].we);
	return;
	}

while(rc==2){
	i--;
	i&=(WEGSIZE-1);

	if(wi[i].ts)
		TcpPrint(skt, 0,"<wi><idx>%02d</idx><dt>%s</dt><ts>%lu</ts><we>%lu</we></wi>",i+1,wi[i].dt,wi[i].ts,wi[i].we);
	if(i==Index)
		break;
	}

while(rc==1){
	i--;
	i&=(WEGSIZE-1);

	if(wi[i].ts>=pat)
		TcpPrint(skt, 0,"<wi><idx>%02d</idx><dt>%s</dt><ts>%lu</ts><we>%lu</we></wi>",i+1,wi[i].dt,wi[i].ts,wi[i].we);
	if(i==Index)
		break;
	}
}

DWORD getWeight(char *weg)
{
DWORD rc;
int cc;
char *data;
data=(char *)malloc(0x100);
	cc=_copy_str_until(data,weg,':')+1;
//	Print("Data(%u): %s\r\n",cc,data);
	cc+=_copy_str_until(data,weg+cc,' ')+1;
//	Print("Data(%u): %s\r\n",cc,data);
	cc+=_copy_str_until(data,weg+cc,'k');
//	Print("Data(%u): %s\r\n",cc,data);
rc=str2long((BYTE *)data);
free((void *) data);
return rc;
}

void Web_fun(int skt, int mode)
{ 
    int cc, err;
    char cm[0x30],arg[0x30];
    char *buf,*cmd;
    if(!mode)
    	Socket0=skt;
    else 
    {
	buf=(char *)malloc(BUFSIZE);
        err = readsocket( skt, buf, BUFSIZE-1);
        if (err <= 0) 
        {              /* error or disconnected by remote side */
            XS_CloseSocket(skt);
        }
	else{
		SOCKET_SET_TCP_KEEP_ALIVE_ON(skt);
		if(!strnicmp(buf,"get",3)){
			cmd=strtok(buf,"/");
			cmd=strtok(NULL," ");
			cc=getCmdNum(cmd,cm,arg);
			if(cc==11)
				DoDateTime(arg);
			if(cc==21)
				TimeZone=str2long(arg);

			printHead(skt,isMode);
			TcpPrint(skt, 0,"<result>%d</result>",cc);
			if(cm[0])
				TcpPrint(skt, 0,"<command>%s</command>",cm);
			if(arg[0])
				TcpPrint(skt, 0,"<argument>%s</argument>",arg);
			
			DoWeight(skt, cc, arg);
			TcpPrint(skt, 1,"</root>");
			}
		else{
			cmd=strtok(buf,"/");
			TcpPrint(skt, 1, "<root><localtime>%lu</localtime><argc>%d</argc><argv>%s</argv></root>",getlocaltime((char *)Answer),err,strtok(NULL," "));
			}

	}
	TcpFlush();
	XS_CloseSocket(skt);
   free(buf);	
   }
}

void CallBackCom(pCOMPORT com, int mode)   
{
struct tm ti;

if(mode) 
	isMode=mode;
   
    switch(mode)
    {
        case 0: // timeout and not access character
            ;          
            break;
        case 1: // receive EndChar     
//            TcpPrint(Socket0, 1, "Receives EndChar, %s\r\n", com->Buf);
		wi[Index].we=getWeight((char *)com->Buf);

		GetTime(&ti.tm_hour,&ti.tm_min,&ti.tm_sec);
		GetDate(&ti.tm_year,&ti.tm_mon,&ti.tm_mday);
                sprintf(wi[Index].dt,"%02u.%02u.%04u %02u:%02u:%02u",ti.tm_mday,ti.tm_mon,ti.tm_year,ti.tm_hour,ti.tm_min,ti.tm_sec);
		_insert_into_string((char *)com->Buf, (char *)wi[Index].dt, 7);
		wi[Index++].ts=getlocaltime((char *)Answer);
		Index&=(WEGSIZE-1);
            break;
        case 2: // access the total character number is the same as TriggerLevel 
//            TcpPrint(Socket0, 1, "Receives the data, %s (data count is same with TriggerLevel, %u)\r\n", com->Buf,com->TriggerLevel);
		;
            break;
        case 3: // the buffer is full 
//            TcpPrint(Socket0, 1, "Buffer is full %u\r\n", com->Bufsize);
		;
            break;
        case 4: // Mastertimeout and no response
//            TcpPrint(Socket0, 1, "Mastertimeout!!\r\n");
		;
            break;
        default:
			//printCom1("[Error mode %d]\n\r",mode);
		isMode=-1;
            break;
    }

    com->Size=0;
}

void cgi_fun(int skt)
{
    int i;
    
    TcpPrint(skt, 0, "<html><head><title></title><head>"
                     "<body bgcolor='#F1F1F1'><center>\n");
    
    if(Argc>1)
    {
        for(i=1; i<Argc; i++)
        {
            if(!strcmp(Argv[i], "status=on"))
		{
                TcpPrint(skt, 0, "<img src='on.gif' border='0' />\n");
		QuitMain=1;
		}
            else if(!strcmp(Argv[i], "status=off"))
                TcpPrint(skt, 0, "<img src='off.gif' border='0' />\n");
        }
    }
    TcpPrint(skt, 0, "<a href='index.htm'><h4>Back</h4></a>\n");
    TcpPrint(skt, 1, "</center></body></html>");
    
    XS_CloseSocket(skt);
}
void default_fun(int skt)
{
    
	TcpPrint(skt, 0, "HTTP/1.1 200 OK\r\nContent-type: text/xml\n\n");
	TcpPrint(skt, 0, "<?xml version=\"1.0\" encoding=\"windows-1251\"?>");
	TcpPrint(skt, 1, "<root><localtime>%lu</localtime><argc>%d</argc><argv>%s</argv></root>",getlocaltime((char *)Answer),Argc,Argv[0]);
    
    XS_CloseSocket(skt);
}

void XS_UserInit(int argc, char *argv[])
{
    extern int bAcceptBroadcast;
    extern unsigned long ACKDELAY;  /* variable in tcp.c */
    extern long MAXTXTOUT;
    void UserLoop(void);
    
    InitLib();
	InstallCom(2,9600UL,8,0,1);

    bAcceptBroadcast=0;
    TcpKeepAliveTime=100;
    ACKDELAY=200;

    XS_AddComPort(&ComRec);
    XS_AddSystemLoopFun(UserLoop);

    XS_AddServer(&TcpServer);   
    // Initialize Web server Kernal
	XS_Http_StartHttpServer();  // Add Web server
//    XS_Http_SetDefaultFile("index.htm");    // Set the default response file
	XS_Http_AddCgi("usercgi", cgi_fun);     // Add CGI function
	XS_Http_AddPathCgi("time",default_fun);
    // Add UDP search function
    XS_AddUdpSocket(&XS_UdpSearch);
    bAcceptBroadcast=1;
    
    MAXTXTOUT=600UL;
    EnableWDT();
    
    XS_AddSystemLoopFun(XS_SocketLoopFun);
    XS_StartSocket();
}

void XS_UserEnd(void)
{
    XS_StopSocket();
    DisableWDT();
    XS_RemoveComPort(&ComRec);
    RestoreCom(2);
}

void UserLoop(void)
{
    RefreshWDT();
    
    if(Kbhit())
        if(Getch()=='q') QuitMain=1;
}

void main(int argc, char *argv[])
{
COUNTDOWNTIMER ctd;
BYTE Lock=0;                  
int i,bSize,cc;
DWORD lb,dwTime;
time_t rawtime;
struct tm *info;

InitLib();
TimerOpen();
Print("LibVersion %04X\r\n",GetLibVersion());
Print("1.coreleft:%lu\r\n",coreleft());
Answer=(BYTE *)malloc(0x100);
Print("2.coreleft:%lu\r\n",coreleft());
T_CountDownTimerStart(&ctd,30000UL);
DelayMs(100U);
Print("IsTimerLeft:%d\r\nTime:%lu\r\n",T_CountDownTimerGetTimeLeft(&ctd),getlocaltime((char *)Answer));

/*lb=getWeight("48273 17/12/2020 17:43:54     1864 kg G");
Print("Brutto:%lu\r\n",lb);
TimerClose();
free((void *)Answer); 
return;
*/
putenv("TZ=GMT+0");
tzset();

InstallCom(2,9600UL,8,0,1);
while(!T_CountDownTimerIsTimeUp(&ctd))
	{
	if(Kbhit())
		{
		free((void *)Answer);
		Print("Bye... coreleft:%lu\r\n",coreleft());
		return;
		}
	if((!(getlocaltime((char *)Answer) % 10UL)) && (!Lock))
		{
		dwTime=getlocaltime((char *)Answer);
		sprintf(Answer,"%lu",dwTime);
		Print("IsTimerLeft:%lu\r\nTime:%lu\r\n",T_CountDownTimerGetTimeLeft(&ctd),dwTime);
		rawtime=(time_t) str2long((unsigned char *)Answer);
		info = gmtime((const time_t*) &rawtime);
		Print("TIME>> %02u.%02u.%u\t%02u:%02u:%02u\r\n",info->tm_mday,info->tm_mon+1,info->tm_year+1900,info->tm_hour,info->tm_min,info->tm_sec);
		Lock=1;
		}
	else if(getlocaltime((char *)Answer) % 10UL)
		Lock=0;	
	if(IsCom(2)==QueueIsNotEmpty)
		{
		bSize=DataSizeInCom(2);
		if(bSize==42)
			{
			Print("COM 2 Data:%i\r\n",bSize);
			for(i=0;i<bSize;i++)
				{
				cc=ReadCom(2);
				if(cc > 0x1F)
					Print("%c",(char)cc);
				}
			Print("\r\n");
			ClearCom(2);
			}
		}
	}
ClearCom(2);
RestoreCom(2);
XS_main(argc, argv);
TimerClose();
free((void *)Answer); 
}