#include <stdio.h>
#include <stdlib.h>
#include <dos.h>

void main(void)
{
struct dostime_t dt;
struct dosdate_t dd;

_dos_getdate( (struct dosdate_t _FAR *) &dd );
_dos_gettime( (struct dostime_t _FAR *) &dt );

printf("#define _RELEASE_ \"%02u:%02u:%02u.%u %02u.%02u.%04u\"",
dt.hour % 24,dt.minute,dt.second,dt.hsecond,dd.day,dd.month,dd.year);
}