#define ArguMAX 40
#define MAXWEBBUFF 2048

int WebInit(void);
int WebSetCOMport(int ComNumber);
int GetWebLiteLibVer(void);

void GetDataFromWeb(unsigned char *return_string,int skt,int Argc,char *Argu_Name[],char *Argu_Str[]);
void EnableSSI(int ivalue);

int GetDataFromForm(int Argc,char *Argu_Name[],char *Argu_Str[]);

int GetJavaScriptFromWeb(char *sFileName,int skt,int Argc,char *Argu_Name[],char *Argu_Str[]);
int Print_CSS(int skt,char *sbgName,char *sCSSName);

int WEBAddCGI(char *filename,void (*cgifun)(int skt,char *sReceive,char *sSend));
int WEB_AddCgi(char cIndex,char *name,char *link,void (*cgifun)(int skt,char *sReceive,char *sSend));
//-------------------------------------------------------------------------------------------------------
int  Get_CGI_Argument_Number(void);
char *Get_CGIKey_By_Index(int index);
char *Get_CGIKey_By_Name(char *name);

void PRINT_XML(char *HtmlFileName);
void PRINT_PAGE(char *HtmlFileName);
/*
  Use on void (*cgifun) called by WEBAddCGI/WEB_AddCgi
*/

void Web_CallBackFun( int skt ,int mode);
/*
 Loop function for Web server
*/

extern int igDisk; //Web server file location (Disk A/B)
extern char bWebEnable; //Web page configuratoin enable/ disable
extern char *SetModuleNametoWeb(char *sname); //MAX length is 10 bytes
extern char *SetAcountPassword(char *password); //MAX length is 7 bytes
extern int PassWordProtect(int iRun);